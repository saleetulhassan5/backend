from django.urls import path

from . import views

urlpatterns = [
    # path('', views.side_page, name='index'),
    path('onload/', views.get_business, name='get_business'),
    path('getdetails/', views.get_pending_details, name='get_pending_details'),
   path('getdata/', views.get_data, name='get_data'),


]
