from django.contrib import admin
from .models import SideMenu, Fields, Project_configs
# Register your models here.

admin.site.register(SideMenu)
admin.site.register(Fields)
admin.site.register(Project_configs)
