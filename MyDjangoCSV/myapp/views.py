import subprocess

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


from django.http import HttpResponse
from django.shortcuts import render
import csv
import os
from django.views.decorators.csrf import csrf_exempt
from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent


@api_view(['GET'])
def get_business(request):

    if request.method == 'GET':
        data_directory = os.path.join(BASE_DIR, 'myapp/data_fetcher/')
        directories = os.listdir(data_directory)
        companies = []
        for directory in directories:
            data = {}
            data['Company'] = directory
            companies.append(data)
        context = {'Companies': companies}
        return Response(context)


@api_view(['POST'])
def get_pending_details(request):

    if request.method == 'POST':
        data = request.data
        business = data['busniess']
        year = data['year']

        data_directory = os.path.join(BASE_DIR, 'myapp/data_fetcher/', business, year)
        directories = os.listdir(data_directory)
        directories.sort(key=int)
        subfolders = []
        for directory in directories:
            subfolders.append(directory)

        context ={'pending': subfolders}
        return Response(context)


@api_view(['POST'])
def get_data(request):

    if request.method == 'POST':
        data = request.data

        business = data['business']
        year = data['year']
        sub_folder = data['sub_folder']

        data_directory = os.path.join(BASE_DIR, 'myapp/data_fetcher/', business, str(year), str(sub_folder))
        directories = os.listdir(data_directory)
        all_data = []
        for directory in directories:
            data = []
            sub_company = {}
            sub_company['sub_company_name'] = directory
            print(directory)
            files_directory = os.path.join(data_directory, directory)

            files = os.listdir(files_directory)
            with open(files_directory + "/" + files[0]) as csv_file:
                data_object = {}
                sub_company['csv'] = files[0]
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    data_object['value'] = row[0]
                    data_object['label'] = row[1]
                    data.append(data_object)

                del data[0]

                sub_company['data'] = data
            all_data.append(sub_company)
    return Response(all_data)






