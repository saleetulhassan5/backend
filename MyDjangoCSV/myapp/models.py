from django.db import models


class SideMenu(models.Model):
    side_item = models.CharField(max_length=200)

    def __str__(self):
        return self.side_item


class Fields(models.Model):
    label = models.CharField(max_length=200)
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.label

class Project_configs(models.Model):
    name = models.CharField(max_length=200)
    main_head = models.CharField(max_length=200)
    sub_head = models.CharField(max_length=200)
